//
//  TagSummaryView.m
//  TestLayer
//
//  Created by Me on 12-01-13.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyView.h"

@implementation MyView
@synthesize myLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
        UIView *v = [[nib instantiateWithOwner:self options:nil] lastObject];
        
        v.frame = self.frame;
        myLabel.text = @"Hello";
        [self addSubview:v];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
